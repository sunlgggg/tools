package spider;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sunlggggg
 * @date 2017/7/25
 */
public class HtmlLoder {


    /**
     * Method: saveToFile
     * Description: save String to file
     *
     * @param filepath file path which need to be saved
     * @param str      string saved
     */
    public static void saveToFile(String filepath, String str) {

        try {
            OutputStreamWriter outs = new OutputStreamWriter(new FileOutputStream(filepath, true), "utf-8");
            outs.write(str);
            System.out.print(str);
            outs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method: UrlConnIsReadToString
     * Description: make InputStream to String
     *
     * @param in_st   inputstream which need to be converted
     * @param charset encoder of value
     * @throws IOException if an error occurred
     */
    public static String UrlConnIsReadToString(InputStream in_st, String charset) throws IOException, IOException {
        BufferedReader buff = new BufferedReader(new InputStreamReader(in_st, charset));
        StringBuffer res = new StringBuffer();
        String line = "";
        while ((line = buff.readLine()) != null) {
            res.append(line);
        }
        return res.toString();
    }

    public static ArrayList<String> getValuableByRegex(String content, String pattern, int groupIndex) {

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(content);
        ArrayList<String> aStrMacthed = new ArrayList<>();
        while (m.find()) {
            aStrMacthed.add(m.group(groupIndex));
        }
        return aStrMacthed;
    }
}
