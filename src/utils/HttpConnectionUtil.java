package utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author sunlggggg
 * @date 2017/7/25
 */
public class HttpConnectionUtil {
    public static synchronized URLConnection getHttpConnection( String url_str ){
        URL url = null;
        try {
            url = new URL(url_str);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        int sec_cont = 1000;
        URLConnection url_con = null ;
        try {
             url_con = url.openConnection();

            url_con.setDoOutput(true);
            url_con.setReadTimeout(10 * sec_cont);
            url_con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url_con;
    }
}
