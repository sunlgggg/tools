package utils;

import java.io.*;
import java.util.ArrayList;

/**
 * @author sunlggggg
 * @date 2017/7/25
 */
public class FileUtil {
    public static FileWriter getFileWriter(String filePath,boolean append){
        File file = new File(filePath);
        if (!file.exists())  {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileWriter fw = null;
        try {
            fw = new FileWriter(file.getAbsoluteFile(),append);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fw;
    }
    public static BufferedWriter getBufferWriter(FileWriter fw){
        BufferedWriter bw = new BufferedWriter(fw);
        return bw;
    }
    public static void WriteToFile(BufferedWriter bw , ArrayList arrayList, boolean isDifferentRow) {
        for (Object s : arrayList) {
            try {
                bw.write((String)s);
                if(isDifferentRow)
                    bw.write("\r\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void Close(Writer writer){
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
