package downloader.thread;

import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;


public class DownloadThread extends Thread {

	public static boolean isCanceled = false;
	private int threadId;

	private long block;
	private URL url;
	private File file;
	private long currentByteNum;
	private boolean isFinished = false;


	public DownloadThread(int threadId, long block, URL url, File file) {
		this.threadId = threadId;
		this.block = block;
		this.url = url;
		this.file = file;
		this.currentByteNum = 0;
	}

	@Override
	public void run(){
		long start = threadId * block;
		long end = (threadId + 1) * block -1;
		try{
			RandomAccessFile accessFile = new RandomAccessFile(file,"rwd");
			accessFile.seek(start);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setConnectTimeout(5000);
			con.setRequestMethod("GET");
			con.setRequestProperty("Range", "bytes="+start+"-"+end);
			if(con.getResponseCode()==206){
				InputStream inStream = con.getInputStream();
				byte[] buffer = new byte[1024];
				int len;
				while( (len = inStream.read(buffer) ) != -1 && (!isCanceled)){
					accessFile.write(buffer,0,len);
					currentByteNum += len;
				}
				accessFile.close();
				inStream.close();
			}
			if(isCanceled)
				System.out.println("第"+(threadId +1)+"条线程已取消");
			else
				System.out.println("第"+(threadId +1)+"条线程下载完毕");
			this.isFinished = true;
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public boolean isFinished() {
		return isFinished;
	}

	public long getCurrentByteNum() {
		return currentByteNum;
	}
}
