package downloader.util;



public class MultiFileDownloader {

	public static void MultiFileDownloader(String[] urls , String savePathDir , String suffix, int threadNum) throws Exception {
		for(String url:urls){
			DownloadUtil.download(url,savePathDir + FileNameGenerator.createtFileName(suffix),threadNum);
		}
	}
}

