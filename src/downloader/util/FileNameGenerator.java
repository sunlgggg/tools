package downloader.util;

import java.text.SimpleDateFormat;

/**
 * Created by sunlggggg on 2017/7/24.
 */
public class FileNameGenerator {
    private static int cnt = 0;
    public static synchronized String createtFileName() {
        return createtFileName("");
    }

    public static synchronized String createtFileName(String suffix) {
        java.util.Date dt = new java.util.Date(System.currentTimeMillis());
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String fileName = fmt.format(dt);
        cnt = (cnt + 1) % 100; //You are free the set %100 to 1000,100000
        StringBuffer sBuf = new StringBuffer("0000").append(cnt);
        sBuf.delete(0, sBuf.length() - 2); //Format length, the file name will be same length
        if (suffix != "")
            fileName = fileName + "_" + sBuf.toString() + "." + suffix;
        return fileName;
    }

    public static void main(String[] args) {
        System.out.println(FileNameGenerator.createtFileName());
    }
}
